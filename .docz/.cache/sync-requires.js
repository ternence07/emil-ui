

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---components-alert-index-mdx": (preferDefault(require("/Users/ternence/Work/WEB/SHXW/emil/emil-ui/components/alert/index.mdx"))),
  "component---components-button-index-mdx": (preferDefault(require("/Users/ternence/Work/WEB/SHXW/emil/emil-ui/components/button/index.mdx"))),
  "component---src-pages-404-js": (preferDefault(require("/Users/ternence/Work/WEB/SHXW/emil/emil-ui/.docz/src/pages/404.js")))
}

