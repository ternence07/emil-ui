import CodeBox from "../../../../../doc-comps/emil-box";
import TypeDemo from "../../../../../components/button/demo/1-type-demo";
import TypeDemoCode from "!raw-loader!../../../../../components/button/demo/1-type-demo";
import IconDemo from "../../../../../components/button/demo/2-icon-demo";
import IconDemoCode from "!raw-loader!../../../../../components/button/demo/2-icon-demo";
import * as React from 'react';
export default {
  CodeBox,
  TypeDemo,
  TypeDemoCode,
  IconDemo,
  IconDemoCode,
  React
};