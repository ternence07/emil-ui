const path = require('path');

exports.onCreateWebpackConfig = args => {
    args.actions.setWebpackConfig({
        resolve: {
            modules: [path.resolve(__dirname, '../src'), 'node_modules'],
            alias: {
                'emil-ui/lib': path.resolve(__dirname, '../components/'),
                'emil-ui/esm': path.resolve(__dirname, '../components/'),
                'emil-ui': path.resolve(__dirname, '../components/'),
            },
        },
    });
};

