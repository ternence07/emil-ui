const { mergeWith } = require('docz-utils')
const fs = require('fs-extra')

let custom = {}
const hasGatsbyConfig = fs.existsSync('./gatsby-config.custom.js')

if (hasGatsbyConfig) {
  try {
    custom = require('./gatsby-config.custom')
  } catch (err) {
    console.error(
      `Failed to load your gatsby-config.js file : `,
      JSON.stringify(err),
    )
  }
}

const config = {
  pathPrefix: '/',

  siteMetadata: {
    title: 'Emil UI',
    description: '自定义组件库',
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-typescript',
      options: {
        isTSX: true,
        allExtensions: true,
      },
    },
    {
      resolve: 'gatsby-theme-docz',
      options: {
        themeConfig: {},
        src: './',
        gatsbyRoot: null,
        themesDir: 'src',
        mdxExtensions: ['.md', '.mdx'],
        docgenConfig: {},
        menu: [],
        mdPlugins: [],
        hastPlugins: [],
        ignore: [],
        typescript: true,
        ts: false,
        propsParser: true,
        'props-parser': true,
        debug: false,
        native: false,
        openBrowser: null,
        o: null,
        open: null,
        'open-browser': null,
        root: '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/.docz',
        base: '/',
        source: './',
        'gatsby-root': null,
        files: './components/**/*.{md,markdown,mdx}',
        public: '/public',
        dest: 'doc-site',
        d: '.docz/dist',
        editBranch: 'master',
        eb: 'master',
        'edit-branch': 'master',
        config: '',
        title: 'Emil UI',
        description: '自定义组件库',
        host: 'localhost',
        port: 3000,
        p: 3000,
        separator: '-',
        paths: {
          root: '/Users/ternence/Work/WEB/SHXW/emil/emil-ui',
          templates:
            '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/node_modules/docz-core/dist/templates',
          docz: '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/.docz',
          cache: '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/.docz/.cache',
          app: '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/.docz/app',
          appPackageJson:
            '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/package.json',
          appTsConfig:
            '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/tsconfig.json',
          gatsbyConfig:
            '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/gatsby-config.js',
          gatsbyBrowser:
            '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/gatsby-browser.js',
          gatsbyNode:
            '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/gatsby-node.js',
          gatsbySSR: '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/gatsby-ssr.js',
          importsJs:
            '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/.docz/app/imports.js',
          rootJs:
            '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/.docz/app/root.jsx',
          indexJs:
            '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/.docz/app/index.jsx',
          indexHtml:
            '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/.docz/app/index.html',
          db: '/Users/ternence/Work/WEB/SHXW/emil/emil-ui/.docz/app/db.json',
        },
      },
    },
  ],
}

const merge = mergeWith((objValue, srcValue) => {
  if (Array.isArray(objValue)) {
    return objValue.concat(srcValue)
  }
})

module.exports = merge(config, custom)
