import * as React from 'react';

export interface Props {
    onClick?: React.MouseEventHandler;
    type?: 'primary' | 'dashed' | 'link' | 'text' | 'ghost' | 'default';
    target?: string;
    size?: 'large' | 'middle' | 'small';
    shape?: 'circle' | 'round' | undefined;
    loading?: boolean | { delay: number };
    icon?: React.ReactNode;
    htmlType?: 'button' | 'submit' | 'reset' | undefined;
    href?: string;
    ghost?: boolean;
    disabled?: boolean;
    danger?: boolean;
    block?: boolean
    className?: string;
    style?: React.CSSProperties;
    children?: React.ReactNode;
    [key: string]: any
}
