import React from 'react';
import { EButton } from 'emil-ui';
import {Space} from "antd";
import 'emil-ui/lib/button/style'

export default () => (
    <Space>
        <EButton type="primary">Primary Button</EButton>
        <EButton>Default Button</EButton>
        <EButton type="dashed">Dashed Button</EButton>
        <br />
        <EButton type="text">Text Button</EButton>
        <EButton type="link">Link Button</EButton>
    </Space>
);
