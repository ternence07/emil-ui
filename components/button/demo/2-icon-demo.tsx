import React from 'react';
import EButton from 'emil-ui/lib/button';
import 'emil-ui/lib/button/style';
import { Space, Tooltip } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

export default () => (
    <>
        <Space>
            <Tooltip title="search">
                <EButton type="primary" shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
            <EButton type="primary" shape="circle">
                A
            </EButton>
            <EButton type="primary" icon={<SearchOutlined />}>
                Search
            </EButton>
            <Tooltip title="search">
                <EButton shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
            <EButton icon={<SearchOutlined />}>Search</EButton>
        </Space>
        <br />
        <br />

        <Space>
            <Tooltip title="search">
                <EButton shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
            <EButton icon={<SearchOutlined />}>Search</EButton>
            <Tooltip title="search">
                <EButton type="dashed" shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
            <EButton type="dashed" icon={<SearchOutlined />}>
                Search
            </EButton>
        </Space>
        <br />
        <br />

        <Space>
            <Tooltip title="search">
                <EButton type="primary" shape="circle" icon={<SearchOutlined />} size="large" />
            </Tooltip>
            <EButton type="primary" shape="circle" size="large">
                A
            </EButton>
            <EButton type="primary" icon={<SearchOutlined />} size="large">
                Search
            </EButton>
            <Tooltip title="search">
                <EButton shape="circle" icon={<SearchOutlined />} size="large" />
            </Tooltip>
            <EButton icon={<SearchOutlined />} size="large">
                Search
            </EButton>
        </Space>
        <br />
        <br />

        <Space>
            <Tooltip title="search">
                <EButton shape="circle" icon={<SearchOutlined />} size="large" />
            </Tooltip>
            <EButton icon={<SearchOutlined />} size="large">
                Search
            </EButton>
            <Tooltip title="search">
                <EButton type="dashed" shape="circle" icon={<SearchOutlined />} size="large" />
            </Tooltip>
            <EButton type="dashed" icon={<SearchOutlined />} size="large">
                Search
            </EButton>
        </Space>
    </>
);
