import React from 'react';
import {Button} from 'antd';

import {Props} from './interface';
import './style'

const EButton: React.FC<Props> = props => {
    const {
        onClick,
        type = 'default',
        target,
        size = 'middle',
        shape,
        loading,
        icon,
        htmlType = 'button',
        href,
        ghost = false,
        disabled = false,
        danger = false,
        block = false,
        className,
        style,
        children,
        ...rest
    } = props;

    return (
        <Button
            type={type}
            target={target}
            size={size}
            shape={shape}
            loading={loading}
            icon={icon}
            htmlType={htmlType}
            href={href}
            ghost={ghost}
            danger={danger}
            block={block}
            className={className}
            style={style}
            onClick={onClick}
            disabled={disabled}
            {...rest}
        >
            {children}
        </Button>
    );
};

export default EButton;
