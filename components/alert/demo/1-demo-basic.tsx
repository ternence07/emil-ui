import React from 'react';
import EAlert from 'emil-ui/lib/alert';
import 'emil-ui/lib/alert/style';

export default () => <EAlert kind="warning"></EAlert>;
