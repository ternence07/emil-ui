import React from 'react';
import { render } from '@testing-library/react';
import EAlert from '..';

describe('<Alert />', () => {
    test('should render default', () => {
        const { container } = render(<EAlert>default</EAlert>);
        expect(container).toMatchSnapshot();
    });

    test('should render alert with type', () => {
        const kinds: any[] = ['info', 'warning', 'positive', 'negative'];

        const { getByText } = render(
            <>
                {kinds.map(k => (
                    <EAlert kind={k} key={k}>
                        {k}
                    </EAlert>
                ))}
            </>,
        );

        kinds.forEach(k => {
            expect(getByText(k)).toMatchSnapshot();
        });
    });
});
